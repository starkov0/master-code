# 3D printing multi-material with Osirix #

This depository contains the code, that allows that bridges Osirix to multi-material 3D printing.
By executing the code, you get a plugin that you have to add to your 32 or 64 bit Osirix software.
The code was developped during my Master Thesis at the University of Geneva, under supervision of Prof. Osman Ratib.

### License ###

This project is available as open source under the terms of the GNU Affero General Public License 3.0, or by explicit permission of the author.
