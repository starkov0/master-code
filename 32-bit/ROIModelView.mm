//
//  View3D.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/03/14.
//
//

#import "ROIModelView.h"

@implementation ROIModelView

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(void) cleanView {
    [self renderer]->RemoveAllViewProps();
    [self setNeedsDisplay:YES];
}

-(void) renderMapper:(vtkSmartPointer<vtkPolyDataMapper>)mapper color:(NSColor*)color {
    
    // actor
    vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor([color redComponent], [color greenComponent], [color blueComponent]);
    actor->GetProperty()->SetOpacity([color alphaComponent]);
    actor->GetProperty()->SetRepresentationToSurface();
        
    // renderer add actor
    [self renderer]->AddActor(actor);
    [self renderer]->SetBackground(0,0,0);
        
    // camera
    vtkCamera *aCamera = [self renderer]->GetActiveCamera();
    aCamera->Zoom(1.5);
    aCamera->SetFocalPoint (0, 0, 0);
    aCamera->SetPosition (0, 0, -1);
    aCamera->ComputeViewPlaneNormal();
    aCamera->SetViewUp(0, -1, 0);
    aCamera->OrthogonalizeViewUp();
    aCamera->SetParallelProjection( false);
    aCamera->SetViewAngle( 60);
    [self renderer]->ResetCamera();
    
    // window
    [self renderWindow]->Start();
    [self setNeedsDisplay:YES];
}

- (void) dealloc
{
    NSLog(@"ROIModelView dealloc");
    
    [self renderer]->RemoveAllViewProps();
    [super dealloc];
}

@end
