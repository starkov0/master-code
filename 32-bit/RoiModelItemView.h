//
//  ItemView.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/06/14.
//
//

#import <Cocoa/Cocoa.h>
@class ROIModel;

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface RoiModelItemView : NSView <NSTextFieldDelegate> {
    ROIModel *model;
    NSTextField *name;
}
@property (retain) ROIModel *model;


///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(void) displayButton;
-(void) setButtonAction;
@end
