//
//  ROIController.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 12/06/14.
//
//

#import <Foundation/Foundation.h>
#import "OsirixAPI/ROIVolumeView.h"
#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/ROI.h"
#import "OsiriXAPI/ITKSegmentation3D.h"
#import "ROIModel.h"
#import "ROIModelView.h"

#include <exception>

@class VTKController;
@class STLController;

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface ROIModelController : NSObject <NSTextFieldDelegate> {
    ViewerController        *vc;
    NSMutableArray          *roiModelArray;
    NSCollectionView        *roiModelCollection;
    ROIModelView            *roiModelView;
    NSArrayController       *roiModelArrayController;
    NSScrollView            *roiModelScrollView;
    NSCollectionView        *roiModelCollectionView;
    
    NSTextField             *smoothingIterationField;
    BOOL                    removeOverlapsState;
    VTKController           *vtkController;
}

// VIEW
@property (assign) IBOutlet NSScrollView *roiModelScrollView;

// ROI MODEL VIEW
@property (assign) IBOutlet ROIModelView *roiModelView;
@property (retain) NSMutableArray *roiModelArray;
@property (assign) IBOutlet NSArrayController *roiModelArrayController;

// SMOOTH
@property (assign) IBOutlet NSTextField *smoothingIterationField;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

// ACTION
- (IBAction)removeOverlaps:(id)sender;

// SAVE FILE
- (IBAction)saveSTL:(id)sender;

// UPDATE VIEW
- (IBAction)updateView:(id)sender;

// MOVE
-(void)moveRoiModel:(ROIModel*)model onTheRightSide:(BOOL)onTheRightSide;

-(void) myDealloc;

@end
