//
//  Viewer2D3D.h
//  PluginTemplate
//
//  Created by pierre starkov on 06.03.14.
//
//

#import <Cocoa/Cocoa.h>

@class MyDCMView;
@class MyVTKView;

@interface MyWindowController : NSWindowController <NSWindowDelegate>{
    MyVTKView *myVTKView;
    MyDCMView *myDCMView;
}
@property (assign) IBOutlet MyVTKView *myVTKView;
@property (assign) IBOutlet MyDCMView *myDCMView;

@end