//
//  MyMainWindowController.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 03/06/14.
//
//

#import <Cocoa/Cocoa.h>
#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/AppController.h"
#import "ROIModelController.h"

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface WindowController : OSIWindowController {
    ROIModelController *roiModelController;
}

// roi model controller
@property (assign) IBOutlet ROIModelController *roiModelController;

@end
