//
//  MyMainWindowController.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 03/06/14.
//
//

#import "WindowController.h"
@class ROIModelController;

@implementation WindowController

@synthesize roiModelController;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

- (id) init
{
    if (self = [super initWithWindowNibName:@"WindowController"])
    {
        [self setMagnetic: YES];
        [[AppController sharedAppController] performSelector: @selector(tileWindows:) withObject:nil afterDelay: 0.1];
    }
    return self;
}

- (void) windowWillCloseNotification: (NSNotification*) notification
{    
	if( [notification object] == [self window] && [[NSUserDefaults standardUserDefaults] boolForKey: @"AUTOTILING"] == YES && magneticWindowActivated == YES)
	{
        [self setMagnetic:NO];
        [NSObject cancelPreviousPerformRequestsWithTarget: [AppController sharedAppController] selector:@selector(tileWindows:) object:nil];
		[[AppController sharedAppController] performSelector: @selector(tileWindows:) withObject:nil afterDelay: 0.3];
    }
    
    [roiModelController myDealloc];
//    [roiModelController release];
}

@end
