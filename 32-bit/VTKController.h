//
//  IsoContourMapper.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 08/06/14.
//
//

#import <Foundation/Foundation.h>

#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/ROI.h"
#import "OsiriXAPI/DCMPix.h"
#import "OsiriXAPI/ITKSegmentation3D.h"
#import <vtkSmartPointer.h>
#import <vtkImageImport.h>
#import <vtkContourFilter.h>
#import <vtkPolyDataNormals.h>
#import <vtkFillHolesFilter.h>
#import <vtkPointData.h>
#import <vtkMarchingCubes.h>
#import <vtkPolyDataConnectivityFilter.h>
#import <vtkWindowedSincPolyDataFilter.h>
#import <vtkImageResample.h>
#import <vtkImageFlip.h>
#import <vtkPolyDataMapper.h>
#import <vtkSTLWriter.h>
#import <vtkTriangleFilter.h>
#import <vtkCleanPolyData.h>
#import <vtkSmoothPolyDataFilter.h>

#include "vtkPolyDataBooleanFilter.h"

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface VTKController : NSObject

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(vtkPolyData*) isoContourFromROIArray:(NSArray*)roiArray;
-(vtkPolyData*) smoothPolyData:(vtkPolyData*)polyData numberOfIteration:(int)numberOfIterations;
-(vtkPolyData*) unionPolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1;
-(vtkPolyData*) intersectionPolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1;
-(vtkPolyData*) differencePolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1;
-(vtkPolyDataMapper*) mapperPolyData:(vtkPolyData*)polyData;
-(void) writePolyData:(vtkPolyData*)polyData STLFile:(NSString*)fileName;

@end
