
//
//  Main.m
//  Plugin
//
//  Created by pierre starkov on 05.03.14.
//
//

#import "Plugin.h"

@implementation Plugin

@synthesize windowController;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

- (void) initPlugin {}

- (long) filterImage:(NSString*) menuName
{
    @try {
        windowController = [[WindowController alloc] init];
        [windowController loadWindow];
        [windowController showWindow:self];
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
        return 0;
    }
    return 0;
}

-(void) dealloc {
    NSLog(@"Plugin dealloc");
    
    [super dealloc];
}

@end
