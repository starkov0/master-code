//
//  ItemView.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/06/14.
//
//

#import "RoiModelItemView.h"
#import "ROIModelController.h"
#import "ROIModel.h"

@implementation RoiModelItemView

@synthesize model;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(void) setButtonAction {
    for (int i=0; i<[[self subviews] count]; i++) {
        if ([[[[self subviews] objectAtIndex:i] identifier] isEqualToString:@"displayedButton"]) {
            [[[self subviews] objectAtIndex:i] setAction:@selector(displayButton)];
            [[[self subviews] objectAtIndex:i] setTarget:self];
        }
        else if ([[[[self subviews] objectAtIndex:i] identifier] isEqualToString:@"innerButton"]) {
            [[[self subviews] objectAtIndex:i] setAction:@selector(innerButtonAction)];
            [[[self subviews] objectAtIndex:i] setTarget:self];
        }
        else if ([[[[self subviews] objectAtIndex:i] identifier] isEqualToString:@"outerButton"]) {
            [[[self subviews] objectAtIndex:i] setAction:@selector(outerButtonAction)];
            [[[self subviews] objectAtIndex:i] setTarget:self];
        }
    }
}

-(void) displayButton {
//    [[model roiModelController] updateView:0];
}

-(void) innerButtonAction {
    [[model roiModelController] moveRoiModel:model onTheRightSide:NO];
}

-(void) outerButtonAction {
    [[model roiModelController] moveRoiModel:model onTheRightSide:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
    NSColor* color = [model color];
    [color setFill];
    NSRectFill(dirtyRect);
    [super drawRect:dirtyRect];
}

- (void)dealloc {
    NSLog(@"RoiModelItemView dealloc");

    [model release];
    [super dealloc];
}

@end
