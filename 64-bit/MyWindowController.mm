//
//  Viewer2D3D.m
//  PluginTemplate
//
//  Created by pierre starkov on 06.03.14.
//
//

#import "MyWindowController.h"

@implementation MyWindowController

@synthesize myVTKView;
@synthesize myDCMView;

- (id) init
{
    self = [super initWithWindowNibName:@"MyWindowController"];
    return self;
}

-(void) windowDidResize:(NSNotification *)notification
{
    NSLog(@"notification: %@", notification);
}


- (void)windowWillClose:(NSNotification *)notification
{
    NSLog(@"notification: %@", notification);
}

@end