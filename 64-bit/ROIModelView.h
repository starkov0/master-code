//
//  View3D.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/03/14.
//
//

#import <vtkSmartPointer.h>
#import <vtkPolyDataAlgorithm.h>
#import "ROIModel.h"
#import "VTKView.h"
#import "VTKController.h"
#import "vtkProperty.h"
#import "vtkCamera.h"

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface ROIModelView : VTKView {}

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(void) cleanView;
-(void) renderMapper:(vtkSmartPointer<vtkPolyDataMapper>)mapper color:(NSColor*)color;
@end
