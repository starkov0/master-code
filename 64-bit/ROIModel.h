//
//  ROIModel.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 12/06/14.
//
//

#import <Foundation/Foundation.h>
#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/ROI.h"
#import "VTKController.h"
@class ROIModelController;

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface ROIModel : NSObject
{
    NSString *name;
    NSColor *color;
    BOOL displayed;
    ROIModelController *roiModelController;
    vtkPolyData *polyData;
}

@property (retain) NSString *name;
@property (retain) NSColor *color;
@property BOOL displayed;
@property (assign) ROIModelController *roiModelController;
@property vtkPolyData *polyData;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------
-(id) initWithController:(ROIModelController*)_roiModelController;

@end
