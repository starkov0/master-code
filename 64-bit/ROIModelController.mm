//
//  ROIController.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 12/06/14.
//
//

#import "ROIModelController.h"
#import "VTKController.h"
#import "ROIModelView.h"

@implementation ROIModelController
@synthesize roiModelScrollView;
@synthesize roiModelView;
@synthesize roiModelArrayController;
@synthesize roiModelArray;
@synthesize smoothingIterationField;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------
-(void) awakeFromNib {
    vc = [ViewerController frontMostDisplayed2DViewer];
    vtkController = [[VTKController alloc] init];
    roiModelArray = [[NSMutableArray alloc] init];
    smoothingIterationField.delegate = self;
    removeOverlapsState = NO;
    [self updateView:0];
}

/////////////////////////////////////////////////////////////// BUTTONS
- (IBAction)updateView:(id)sender {
    [self reloadROIs];
    [self removeOverlapsAction];
    [self smoothModels];
    [self renderView];
}

- (IBAction)removeOverlaps:(id)sender {
    removeOverlapsState = !removeOverlapsState;
}

- (BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor doCommandBySelector:(SEL)commandSelector {
    BOOL retVal = NO;
    if (commandSelector == @selector(insertNewline:)) {
        retVal = YES;
        if (control == smoothingIterationField) {
            [self updateView:0];
        }
    }
    return retVal;
}

/////////////////////////////////////////////////////////////// REALOAD
-(void) reloadROIs {
    NSMutableArray *roiNames = [self roiNames];
    NSMutableArray *roiDisplayed = [self roiDisplayedWithRoiNames:roiNames];
    
    [roiModelArrayController removeObjects:roiModelArray];
    
    if (vc != NULL) {
        NSString *waitString = [NSString stringWithFormat:@"3D Printing: Preparing"];
        WaitRendering *wait = [self waitRenderingStartWithString:waitString];
        
        if ([roiNames count]) {
            for (int i=0; i<[roiNames count]; i++) {
                NSMutableArray *roiArray = [self roiArrayWithName:[roiNames objectAtIndex:i]];
                NSColor * roiColor = [self colorWithName:[roiNames objectAtIndex:i]];
                ROIModel *roiModel = [[ROIModel alloc] initWithController:self];
                [roiModel setName:[roiNames objectAtIndex:i]];
                [roiModel setPolyData:[vtkController isoContourFromROIArray:roiArray]];
                [roiModel setDisplayed:[[roiDisplayed objectAtIndex:i] boolValue]];
                [roiModel setColor:roiColor];
                [roiModelArrayController addObject:roiModel];
                [roiModel release];
            }
        }
        
        [self waitRenderingEnd:wait];
    }
    
}

-(NSMutableArray*) roiNames {
    NSMutableArray *current_names = [NSMutableArray array];
    NSMutableArray *names_to_remove = [NSMutableArray array];
    
    // get current names
    for (ROIModel *model in roiModelArray) {
        [current_names addObject:[model name]];
    }
    
    // remove current names no more used
    for (NSString *name in current_names) {
        if (![[vc roiNames] containsObject:name]) {
            [names_to_remove addObject:name];
        }
    }
    [current_names removeObjectsInArray:names_to_remove];
    
    // add new names
    for (NSString *name in [vc roiNames]) {
        if (![current_names containsObject:name]) {
            [current_names addObject:name];
        }
    }
    return current_names;
}

-(NSMutableArray*) roiDisplayedWithRoiNames:(NSMutableArray*)roiNames {
    NSMutableArray *roiDisplayed = [NSMutableArray array];
    
    for (NSString *name in roiNames) {
        BOOL nameFound = NO;
        for (int i=0; i<[roiModelArray count]; i++) {
            if ([[[roiModelArray objectAtIndex:i] name] isEqual:name]) {
                nameFound = YES;
                if ([[roiModelArray objectAtIndex:i] displayed]) {
                    [roiDisplayed addObject:[NSNumber numberWithBool:YES]];
                }
                else {
                    [roiDisplayed addObject:[NSNumber numberWithBool:NO]];
                }
                break;
            }
        }
        if (!nameFound) {
            [roiDisplayed addObject:[NSNumber numberWithBool:YES]];
        }
    }
    return roiDisplayed;
}

-(NSMutableArray*) roiArrayWithName:(NSString*) name {
    // init array
    NSMutableArray *roiArray = [NSMutableArray array];
    for (int i=0; i<[[vc roiList] count]; i++){
        [roiArray addObject:[NSMutableArray array]];
    }
    // fill with ROI
    for (int i=0; i<[[vc roiList] count]; i++){
        for (int j=0; j<[[[vc roiList] objectAtIndex:i] count]; j++){
            ROI *roi = [[[vc roiList] objectAtIndex:i] objectAtIndex:j];
            if ([name isEqualToString:[roi name]]) {
                [[roiArray objectAtIndex:i] addObject:roi];
            }
        }
    }
    return roiArray;
}

-(NSColor*) colorWithName:(NSString*)name {
    // init
    BOOL stop = NO;
    NSColor *color = NULL;
    // fill color values from ROI
    for (int i=0; i<[[vc roiList] count]; i++){
        for (int j=0; j<[[[vc roiList] objectAtIndex:i] count]; j++){
            ROI *roi = [[[vc roiList] objectAtIndex:i] objectAtIndex:j];
            if ([name isEqualToString:[roi name]]) {
                color = [NSColor colorWithCalibratedRed:[roi rgbcolor].red/65535.0f
                                                  green:[roi rgbcolor].green/65535.0f
                                                   blue:[roi rgbcolor].blue/65535.0f
                                                  alpha:[roi opacity]];
                stop = YES;
                break;
            }
        }
        if (stop) {
            break;
        }
    }
    if (color == NULL) {
        color = [NSColor colorWithCalibratedRed:1.0f
                                          green:1.0f
                                           blue:1.0f
                                          alpha:1.0f];
    }
    return color;
}

/////////////////////////////////////////////////////////////// SMOOTH
-(void) smoothModels {
    int smoothingIterationFieldValue = [smoothingIterationField integerValue] * 2;
    
    if (smoothingIterationFieldValue <= 0) {
        [smoothingIterationField setIntegerValue:0];
    }
    else {
        NSString *waitString = [NSString stringWithFormat:@"3D Printing: Smoothing"];
        WaitRendering *wait = [self waitRenderingStartWithString:waitString];
        
        for (ROIModel *model : roiModelArray){
            [model setPolyData:[vtkController smoothPolyData:[model polyData] numberOfIteration:smoothingIterationFieldValue]];
        }

        [self waitRenderingEnd:wait];
    }
}

/////////////////////////////////////////////////////////////// CONSTRUCTIVE SOLID GEOMETRY
- (void)removeOverlapsAction {
    [roiModelView cleanView];

    if (removeOverlapsState && [roiModelArray count] > 1) {
        
        NSString *waitString = [NSString stringWithFormat:@"3D Printing: Removing Overlaps"];
        WaitRendering *wait = [self waitRenderingStartWithString:waitString];
        
        vtkPolyData *polyData, *unionPolyData;
        
        for (int i=0; i<[roiModelArray count]; i++) {
            unionPolyData = [[roiModelArray objectAtIndex:i] polyData];
            for (int j=i; j<[roiModelArray count]; j++) {
                if (j > i) {
                    polyData = [[roiModelArray objectAtIndex:j] polyData];
                    try {
                        [[roiModelArray objectAtIndex:j] setPolyData:[vtkController differencePolyData:polyData :unionPolyData]];
                        unionPolyData = [vtkController unionPolyData:polyData :unionPolyData];
                    } catch (char const *error) {
                        NSLog(@"error in unionPolyData %d %@ %s", i, [[roiModelArray objectAtIndex:i] name], error);
                    }
                }
            }
        }
        
        [self waitRenderingEnd:wait];
    }
}

/////////////////////////////////////////////////////////////// RENDERING
-(void) renderView {
    NSString *waitString = [NSString stringWithFormat:@"3D Printing: Rendering"];
    WaitRendering *wait = [self waitRenderingStartWithString:waitString];
    
    for (ROIModel *model in roiModelArray) {
        if ([model displayed]) {
            vtkPolyDataMapper* mapper = [vtkController mapperPolyData: [model polyData]];
            [roiModelView renderMapper:mapper color:[model color]];
        }
    }
    
    [self waitRenderingEnd:wait];
}

/////////////////////////////////////////////////////////////// MOVE ROI MODEL
-(void)moveRoiModel:(ROIModel*)model onTheRightSide:(BOOL)onTheRightSide
{
    int position = [roiModelArray indexOfObject:model];
    
    if (onTheRightSide) {
        if (position < [roiModelArray count]-1) {
            ROIModel *model_0 = [roiModelArray objectAtIndex:position];
            ROIModel *model_1 = [roiModelArray objectAtIndex:position+1];
            
            [roiModelArrayController removeObjectAtArrangedObjectIndex:position];
            [roiModelArrayController removeObjectAtArrangedObjectIndex:position];
            
            [roiModelArrayController insertObject:model_1 atArrangedObjectIndex:position];
            [roiModelArrayController insertObject:model_0 atArrangedObjectIndex:position+1];
        }
    }
    else {
        if (position > 0) {
            ROIModel *model_0 = [roiModelArray objectAtIndex:position];
            ROIModel *model_1 = [roiModelArray objectAtIndex:position-1];
            
            [roiModelArrayController removeObjectAtArrangedObjectIndex:position-1];
            [roiModelArrayController removeObjectAtArrangedObjectIndex:position-1];
            
            [roiModelArrayController insertObject:model_0 atArrangedObjectIndex:position-1];
            [roiModelArrayController insertObject:model_1 atArrangedObjectIndex:position];
        }
    }
}

/////////////////////////////////////////////////////////////// SAVE FILE
- (IBAction)saveSTL:(id)sender {
    for (ROIModel *model in roiModelArray) {
        NSSavePanel *panel = [NSSavePanel savePanel];
        [panel setCanCreateDirectories:YES];
        [panel setCanSelectHiddenExtension:NO];
        [panel setAllowedFileTypes:[[NSArray alloc] initWithObjects:@"stl",nil]];
        [panel setAllowsOtherFileTypes:NO];
        [panel setNameFieldStringValue:[NSString stringWithFormat:@"%@.stl", [model name]]];
        [panel beginWithCompletionHandler:^(NSInteger result) {
            if (result == NSFileHandlingPanelOKButton) {
                [vtkController writePolyData:[model polyData] STLFile:[[panel URL ] path]];
            }
        }];
    }
}

/////////////////////////////////////////////////////////////// WAIT RENDERING
-(WaitRendering*) waitRenderingStartWithString:(NSString*)waitString {
    WaitRendering *wait = [[WaitRendering alloc] init: NSLocalizedString(waitString, nil)];
    [wait start];
    return wait;
}

-(void) waitRenderingEnd:(WaitRendering*)wait {
    [wait end];
    [wait close];
    [wait autorelease];
}

/////////////////////////////////////////////////////////////// DEALLOC
-(void) myDealloc {
    [roiModelArrayController removeObjects:roiModelArray];
}

- (void) dealloc {
    
    NSLog(@"ROIModelController dealloc");

    [roiModelArray release];
    [roiModelCollection release];
    [roiModelCollectionView release];
    [vtkController release];
    [super dealloc];
}

@end
