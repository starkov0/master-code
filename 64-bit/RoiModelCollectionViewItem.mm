//
//  CollectionViewItem.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/06/14.
//
//

#import "RoiModelCollectionViewItem.h"
#import "RoiModelItemView.h"

@implementation RoiModelCollectionViewItem

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)setRepresentedObject:(id)object {
    [super setRepresentedObject:object];
    if (object != NULL){
        RoiModelItemView *view = (RoiModelItemView *)[self view];
        [view setModel:object];
        [view setButtonAction];
    }
}

-(void) dealloc {
    NSLog(@"RoiModelCollectionViewItem dealloc");
    
    [super dealloc];
}

@end
