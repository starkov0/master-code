//
//  CollectionViewItem.h
//  PluginTemplate
//
//  Created by Pierre Starkov on 13/06/14.
//
//

#import <Cocoa/Cocoa.h>

///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface RoiModelCollectionViewItem : NSCollectionViewItem

@end
