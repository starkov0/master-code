//
//  IsoContourMapper.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 08/06/14.
//
//

#import "VTKController.h"

@implementation VTKController

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

-(vtkPolyData*) isoContourFromROIArray:(NSArray*)roiArray
{
    // initialisation
    NSData *vD = nil;
    NSMutableArray *copyPixList = nil;
    ViewerController *vc = [ViewerController frontMostDisplayed2DViewer];
    [vc copyVolumeData: &vD andDCMPix: &copyPixList forMovieIndex: vc.curMovieIndex];
    
    // copying
    for( DCMPix *p in copyPixList) {
        memset( p.fImage, 0, p.pheight*p.pwidth*sizeof( float));
    }
    
    for( int z = 1; z < [copyPixList count]-1; z++) {
        DCMPix *p = [copyPixList objectAtIndex: z];
        for (int i=0; i<[[roiArray objectAtIndex:z] count]; i++) {
            ROI *roi = [[roiArray objectAtIndex:z] objectAtIndex:i];
            [p fillROI: roi
                newVal:1000
              minValue:-FLT_MAX
              maxValue:FLT_MAX
               outside:NO
      orientationStack:2
               stackNo:0
               restore:NO
              addition:NO
                spline:[roi isSpline]
               clipMin:NSMakePoint(1,1)
               clipMax:NSMakePoint(p.pwidth-1, p.pheight-1)];
        }
    }
    
    // some importation
    vtkImageImport *isoReader = vtkImageImport::New();
    isoReader->SetWholeExtent(0, [copyPixList.lastObject pwidth]-1, 0, [copyPixList.lastObject pheight]-1, 0, copyPixList.count-1);
    isoReader->SetDataExtentToWholeExtent();
    isoReader->SetDataScalarTypeToFloat();
    isoReader->SetImportVoidPointer( (void*) [vD bytes]);
    isoReader->SetDataSpacing( [copyPixList.lastObject pixelSpacingX], [copyPixList.lastObject pixelSpacingY], [copyPixList.lastObject sliceInterval]);
    
    // iso contour
    vtkContourFilter *isoContour = vtkContourFilter::New();
    isoContour->SetInputConnection(isoReader->GetOutputPort());
    isoContour->SetValue(0, 500);
    
    vtkPolyDataNormals *normals = vtkPolyDataNormals::New();
    normals->SetInput(isoContour->GetOutput());
    normals->SetFeatureAngle(120);
    return normals->GetOutput();
}

-(vtkPolyData*) smoothPolyData:(vtkPolyData*)polyData numberOfIteration:(int)numberOfIterations
{
    vtkWindowedSincPolyDataFilter *smoother = vtkWindowedSincPolyDataFilter::New();
    smoother->SetInput(polyData);
    smoother->SetNumberOfIterations(numberOfIterations);
    smoother->BoundarySmoothingOn();
    smoother->FeatureEdgeSmoothingOn();
    smoother->SetFeatureAngle(120.0);
    smoother->SetPassBand(.001);
    smoother->NonManifoldSmoothingOn();
    smoother->NormalizeCoordinatesOn();
    smoother->Update();
    
    vtkPolyDataNormals *normals = vtkPolyDataNormals::New();
    normals->SetInput(smoother->GetOutput());
    normals->SetFeatureAngle(120);
    return normals->GetOutput();
}

-(vtkPolyData*) unionPolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1
{
    vtkPolyDataBooleanFilter *booleanOperation = vtkPolyDataBooleanFilter::New();
    booleanOperation->SetOperMode(0);
    booleanOperation->SetInput(0, polyData_0);
    booleanOperation->SetInput(1, polyData_1);
    booleanOperation->Update();
    return booleanOperation->GetOutput();
}

-(vtkPolyData*) intersectionPolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1
{
    vtkPolyDataBooleanFilter *booleanOperation = vtkPolyDataBooleanFilter::New();
    booleanOperation->SetOperMode(1);
    booleanOperation->SetInput(0, polyData_0);
    booleanOperation->SetInput(1, polyData_1);
    booleanOperation->Update();
    return booleanOperation->GetOutput();
}

-(vtkPolyData*) differencePolyData:(vtkPolyData*)polyData_0 :(vtkPolyData*)polyData_1
{
    vtkPolyDataBooleanFilter *booleanOperation = vtkPolyDataBooleanFilter::New();
    booleanOperation->SetOperMode(2);
    booleanOperation->SetInput(0, polyData_0);
    booleanOperation->SetInput(1, polyData_1);
    booleanOperation->Update();
    return booleanOperation->GetOutput();
}

-(vtkPolyDataMapper*) mapperPolyData:(vtkPolyData*)polyData
{
    vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
    mapper->SetInput(polyData);
    mapper->ScalarVisibilityOff();
    mapper->Update();
    return mapper;
}

-(void) writePolyData:(vtkPolyData*)polyData STLFile:(NSString*)fileName
{
    vtkSTLWriter *stlWriter = vtkSTLWriter::New();
    stlWriter->SetFileName([fileName UTF8String]);
    stlWriter->SetInput(polyData);
    stlWriter->Write();
}

-(void) dealloc {
    NSLog(@"VTKController dealloc");

    [super dealloc];
}


@end
