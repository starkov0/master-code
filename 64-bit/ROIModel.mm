//
//  ROIModel.m
//  PluginTemplate
//
//  Created by Pierre Starkov on 12/06/14.
//
//

#import "ROIModel.h"
#import "ROIModelController.h"

@implementation ROIModel

@synthesize name;
@synthesize color;
@synthesize displayed;
@synthesize polyData;
@synthesize roiModelController;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

// INIT
-(id) initWithController:(ROIModelController*)_roiModelController
{
    self = [super init];
    roiModelController = _roiModelController;
    return self;
}

-(void) dealloc
{
    NSLog(@"ROIModel dealloc");

    [name release];
    [color release];
    [super dealloc];
}

@end
