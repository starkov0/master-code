//
//  Main.h
//  Plugin
//
//  Created by pierre starkov on 05.03.14.
//
//

#import <Foundation/Foundation.h>
#import "WindowController.h"
#import "OsiriXAPI/PluginFilter.h"
#import "OsiriXAPI/ITKBrushROIFilter.h"
#import "OsiriXAPI/ViewerController.h"
#import "OsiriXAPI/ITKSegmentation3DController.h"


///-----------------------------------------------------------
/// @name INTERFACE
///-----------------------------------------------------------

@interface Plugin : PluginFilter
{
    WindowController  *windowController;
}

// window controller
@property (retain) WindowController   *windowController;

///-----------------------------------------------------------
/// @name FUNCTIONS
///-----------------------------------------------------------

- (void) initPlugin;
- (long) filterImage:(NSString*) menuName;

@end